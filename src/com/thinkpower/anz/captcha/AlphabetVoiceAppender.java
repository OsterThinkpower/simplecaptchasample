package com.thinkpower.anz.captcha;

import java.io.File;
import java.io.IOException;
import java.io.SequenceInputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AlphabetVoiceAppender {
	
	public static AudioInputStream generateVoice(String answer) throws UnsupportedAudioFileException, IOException {
		if (answer == null)
			throw new IllegalArgumentException("Parameter answer is null.");
		
		AudioInputStream inputStreamAppendedFiles = null;
		
		for (int i = 0; i < answer.length(); i++) {
			char alphabet = Character.toUpperCase(answer.charAt(i));
			//char alphabet = answer.charAt(i);
			
			if (inputStreamAppendedFiles == null) {
				inputStreamAppendedFiles = AudioSystem.getAudioInputStream(AlphabetVoiceAppender.class.getResource("/sound/tp/alphabets/" + alphabet + ".wav"));
			} else {
				AudioInputStream targetIs = AudioSystem.getAudioInputStream(AlphabetVoiceAppender.class.getResource("/sound/tp/alphabets/" + alphabet + ".wav"));
				inputStreamAppendedFiles = new AudioInputStream(
						new SequenceInputStream(inputStreamAppendedFiles, targetIs),
						inputStreamAppendedFiles.getFormat(),
						inputStreamAppendedFiles.getFrameLength() + targetIs.getFrameLength()
				);
			} 						
		}
		
		return inputStreamAppendedFiles;
	}
	
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException {
		AudioInputStream is = generateVoice("cxgrjp");
		
		AudioSystem.write(is, 
                AudioFileFormat.Type.WAVE, 
                new File(System.getProperty("user.home") + "/wavAppended.wav"));
	}
	
}
