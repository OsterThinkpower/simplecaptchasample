package com.thinkpower.anz.captcha;

import nl.captcha.audio.Sample;
import nl.captcha.audio.producer.VoiceProducer;
import nl.captcha.util.FileUtil;

/**
 * <p>
 * {@link VoiceProducer} which generates a vocalization for a given number,
 * randomly selecting from a list of voices. The default voices are located in
 * the jar or source package in the <code>sounds/oster/alphabets</code> directory, and have filenames
 * with a format of <i>alphabet</i>.wav, e.g.:
 * <code>sounds/oster/alphabets/b.wav</code>.
 * </p>
 * 
 * @author <a href="mailto:james.childers@gmail.com">James Childers</a>
 * 
 */
public class AlphabetVoiceProducer implements VoiceProducer {

    public AlphabetVoiceProducer() {
    }

    @Override public final Sample getVocalization(char alphabet) {    	
        return FileUtil.readSample("/sound/oster/alphabets/" + alphabet + ".wav");
    	//return FileUtil.readSample("/sound/tp/alphabets/" + Character.toUpperCase(alphabet) + ".wav");
    }
}
