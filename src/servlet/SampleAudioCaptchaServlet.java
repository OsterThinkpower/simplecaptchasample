package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkpower.anz.captcha.AlphabetVoiceProducer;

import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.TextProducer;

/**
 * Servlet implementation class AudioCaptchaServlet
 */
public class SampleAudioCaptchaServlet extends HttpServlet {
       
	private static final long serialVersionUID = 4690256047223360039L;

    @Override protected void doGet(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException, IOException {
    	Captcha imageCaptcha = (Captcha) req.getSession().getAttribute(Captcha.NAME);

        AudioCaptcha ac = new AudioCaptcha.Builder()
            .addAnswer(new TextProducer() {
				@Override
				public String getText() {
					System.out.println("Image Captcha answer is " + imageCaptcha.getAnswer());
					return imageCaptcha.getAnswer(); 
				}
			})
            .addVoice(new AlphabetVoiceProducer())
            //.addNoise()
            .build();

        CaptchaServletUtil.writeAudio(resp, ac.getChallenge());
    }

    @Override protected void doPost(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
