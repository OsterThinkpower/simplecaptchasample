package servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.thinkpower.anz.captcha.AlphabetVoiceAppender;
import com.thinkpower.anz.captcha.AlphabetVoiceProducer;

import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.TextProducer;

/**
 * Servlet implementation class AudioCaptchaServlet
 */
public class AudioCaptchaServlet extends HttpServlet {
       
	private static final long serialVersionUID = 4690256047223360039L;

    @Override protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    	Captcha imageCaptcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
        
		System.out.println("Image Captcha answer is " + imageCaptcha.getAnswer());
		AudioInputStream answerAudioIs;
		try {
			answerAudioIs = AlphabetVoiceAppender.generateVoice(imageCaptcha.getAnswer());
		} catch (UnsupportedAudioFileException e) {
			throw new ServletException(e);
		}

		//
		response.setHeader("Cache-Control", "private,no-cache,no-store");
        response.setContentType("audio/wave");
        response.setContentLength((int) answerAudioIs.getFrameLength());
        System.out.println("Audio file length:" + answerAudioIs.getFrameLength());
        
        //ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        AudioSystem.write(answerAudioIs,
                AudioFileFormat.Type.WAVE, response.getOutputStream());
        

//        byte[] buffer = new byte[8092];
//
//        for (int length = 0; (length = answerAudioIs.read(buffer)) > 0;) {
//        	response.getOutputStream().write(buffer, 0, length);
//        }
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

    @Override protected void doPost(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
