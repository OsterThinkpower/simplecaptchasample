package servlet;

import java.awt.Color;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.captcha.Captcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.gimpy.FishEyeGimpyRenderer;
import nl.captcha.gimpy.RippleGimpyRenderer;
import nl.captcha.noise.StraightLineNoiseProducer;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.producer.ArabicTextProducer;
import nl.captcha.text.producer.DefaultTextProducer;

/**
 * Servlet implementation class ImageCaptchaServlet
 */
public class ImageCaptchaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageCaptchaServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Captcha captcha = new Captcha.Builder(200, 50)
				 .addText(new DefaultTextProducer(6, new char[] { 'a', 'b', 'c', 'd',
				            'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'r', 'w', 'x', 'y'}))
				 .addBackground(new GradiatedBackgroundProducer())
				 .addNoise(new StraightLineNoiseProducer(new Color(0.7f, 0.4f, 0.6f, 1f), 4))
				 .gimp(new FishEyeGimpyRenderer(
						 new Color(0.7f, 0.7f, 0.7f, 0.4f), 
						 new Color(0.7f, 0.7f, 0.7f, 0.4f)))
				 .addBorder()
				 .build();
		 
		CaptchaServletUtil.writeImage(response, captcha.getImage());
		request.getSession().setAttribute(Captcha.NAME, captcha);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
