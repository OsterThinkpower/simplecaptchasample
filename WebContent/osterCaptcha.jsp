<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SimpleCAPTCHA Example</title>
    <link href="sc.css" type="text/css" rel="stylesheet" />
    
    <script>
    function loadAudio() {
        document.getElementById("audioCaptcha").src = "AudioCaptchaServlet?bogus=" + new Date().getTime();
    }
    </script>
</head>
<body>
<br>

<h3>Oster Play CAPTCHA</h3>
<img src="<c:url value="ImageCaptchaServlet" />"><br />

<audio controls autoplay id="audioCaptcha"></audio>
<button onclick="loadAudio()">Audio</button>

<form action="captchaSubmit.jsp" method="post">
	<input type="text" name="answer"></input>
	<input type="submit"></input>
</form>

</body>
</html>
