# Simple captcha sample #

This is sample source to use the Simple captcha module.

### Description ###
The sample code is modified from the Simple captcha.
Add two servlet to create customized image/audio captcha.
Add one JSP to combine image/audio on the one page.

### Dependency library ###

* [simplecaptcha-latest.jar](https://bitbucket.org/OsterThinkpower/simplecaptchasample/src/f20477caa32065da7a8a0cc68aa76cac27a302b9/WebContent/WEB-INF/lib/simplecaptcha-latest.jar?at=master)

### Sample Source Code ###
image/audio captcha with the same answer [JSP Sample](https://bitbucket.org/OsterThinkpower/simplecaptchasample/src/f20477caa32065da7a8a0cc68aa76cac27a302b9/WebContent/osterCaptcha.jsp?at=master)

[Audio Captcha Servlet](https://bitbucket.org/OsterThinkpower/simplecaptchasample/src/f20477caa32065da7a8a0cc68aa76cac27a302b9/src/servlet/AudioCaptchaServlet.java?at=master)

[Image Captcha Servlet](https://bitbucket.org/OsterThinkpower/simplecaptchasample/src/f20477caa32065da7a8a0cc68aa76cac27a302b9/src/servlet/ImageCaptchaServlet.java?at=master)

### Result ###
![螢幕快照 2016-11-08 下午6.52.15.png](https://bitbucket.org/repo/9EKAGL/images/3374771614-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-11-08%20%E4%B8%8B%E5%8D%886.52.15.png)